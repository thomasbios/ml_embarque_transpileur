import joblib

def predict():
    model = joblib.load("model.joblib")
    thetas = model.coef_
    bias = model.intercept_

    thetas_str = "{"
    len_thetas = len(thetas)
    for i in range(len_thetas - 1):
        thetas_str += str(thetas[i])
        thetas_str += ","
    thetas_str += str(thetas[len_thetas - 1])
    thetas_str += "}"

    c_function = f"""
    #include <stdio.h>
    #include <stdlib.h> \n

    float linear_regression_prediction(float* features, int n_feature)
    {{
        float res = {bias};
        int n_thetas = {len_thetas};
        float thetas[] = {thetas_str};
        for (int i = 0; i < n_thetas; i++)
        {{
            res += features[i] * thetas[i];
        }}
        return res;
    }}\n
    """
    return c_function

def main():
    c_function = f"""
    int main(int argc, char *argv[])
    {{
        if (argc != 3)
            return 1;
        int n_features = argc - 1;
        char *first_features = argv[1];
        char *second_features = argv[2];
        float array[] = {{atof(first_features), atof(second_features)}};
        printf(\"%f\\n\", linear_regression_prediction(array, n_features));
    }}
    """
    return c_function

if __name__ == '__main__':
    f = open("linear_regression.c", "w")
    f.write(predict())
    f.write(main())
    f.close()
