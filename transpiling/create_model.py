import pandas as pd
import joblib
from sklearn.linear_model import LinearRegression

df = pd.read_csv("tumors.csv")

model = LinearRegression()

X_train = df[["size", "p53_concentration"]]
y_train = df["is_cancerous"]

model.fit(X_train, y_train)

f = open("model.joblib", 'wb')
joblib.dump(model, f)
f.close()
