# ML Embarque exercices

## Authors

thomas.alia

# Transpiling

Go the the transpiling folder.

First you should create a virtual env :

`python -m venv venv`
`source venv/bin/activate`

Then install dependencies

`pip instal -r requirements.txt`

### Use

You must have a model.joblib in your root, launch the following command to create it :

`python create_model.py`

You then can launch the following command to create the corresponding c code.

`python transpiling.py`

then to compile the c code:

`gcc transpiling.py -o regression`

You now have the final binary, call it with two argument:

`./regression [arg1] [arg2]`

# Quantization

The quantization notebook is available in the `quantization` folder

# Web Service

Go to the ML_prod_1_web_service folder

Create a venv and install dependencies

Launch the server with `python model_api.py`

