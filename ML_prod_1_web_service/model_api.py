from fastapi import FastAPI
import json
from typing import *
from routes.predict import XData, Prediction
from routes.tumors import TumorPredict
from models.result import ResultModel

app = FastAPI()


@app.get('/test_predict')
async def test_predict():
    return ResultModel([1, 2, 3]).get_json()


@app.post('/predict')
async def predict(X: XData):
    pred = Prediction()
    return pred.calculate_prediction(X)


@app.post('/tumor_predict')
async def tumor_predict(X: XData):
    pred = TumorPredict()
    return pred.calculate_prediction(X)