import requests

r = requests.get('http://localhost:8000/test_predict')

print(r.status_code)
print(r.json())

r = requests.post('http://localhost:8000/predict',
                  data="{\"X\": [\"022\", \"2\", \"3\"]}")
print(r.status_code)
print(r.json())

r = requests.post('http://localhost:8000/tumor_predict',
                  data="{\"X\": [\"022\", \"2\", \"3\"]}")
print(r.status_code)
print(r.json())