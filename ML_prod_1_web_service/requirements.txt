fastapi==0.70.0
uvicorn==0.15.0
requests==2.26.0
joblib==1.1.0
sklearn