import json
from typing import *


class ResultModel:
    def __init__(self, y: List[int]):
        self.y = list(y)

    def get_json(self) -> str:
        return json.dumps(self.__dict__)
