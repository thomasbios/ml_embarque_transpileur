import numpy as np
from typing import *
from models.result import ResultModel
from pydantic import BaseModel


class XData(BaseModel):
    X: List[int]


class TumorPredict:
    def calculate_prediction(self, X: XData):
        return "oui"