from pydantic import BaseModel
from joblib import load
import numpy as np
from typing import *
from models.result import ResultModel


class XData(BaseModel):
    X: List[int]


class Prediction:
    def __init__(self):
        self._model_path = "data/regression.joblib"
        self.model = load(self._model_path)

    def calculate_prediction(self, X: XData):
        res = self.model.predict(np.array(X.X).reshape(1, -1))
        model_res = ResultModel(res)
        print(model_res.get_json())
        return model_res.get_json()
